"""
A simple example (programmed procedurally)

See PyQt5 Docs:
 http://pyqt.sourceforge.net/Docs/PyQt5/introduction.html
 https://www.riverbankcomputing.com/static/Docs/PyQt5/module_index.html

Note: the C++ Qt class reference can be more usable than the PyQt5 one:
 https://doc.qt.io/qt-5/classes.html

"""

import sys
from PyQt5 import Qt


def printText(text):
    """a dummy function to be used as a slot for a connection"""
    print(text)


# Initialize a Qt application (Qt will crash if you do not do this first)
app = Qt.QApplication(sys.argv)

# instantiate a widget (note that QLineEdit inherits QWidget)
w = Qt.QLineEdit()

# connect signal
w.textChanged.connect(printText)

# call a method of the widget
w.setText("This is a PyQt5 QLineEdit widget")

# show it (if you do not show the widget, it won't be visible)
w.show()

# Initialize the Qt event loop (and exit when we close the app)
sys.exit(app.exec_())
