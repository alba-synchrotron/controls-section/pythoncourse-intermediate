# Python Intermediate Course

This project contains materials and resources related to the Python Intermediate
course (Object Oriented Programming, Qt and Taurus) at ALBA:


## Goal of the course

At the end of this course you should be able to:

- move ahead from simple structured programming (linear code using funcions) to
  Object-Oriented Programming in your python programs (understanding classes, 
  objects, inheritance,...)
- create your own simple GUIs with PyQt
- be able to modify and write from scratch your own Taurus Widgets
- be autonomous to learn more complex techniques on your own

For more details, look at the course contents below.

## Course contents:

- [docs](docs): Course index, documents and links of interest for the course 
  here.

- Proposed [exercises](exercises) and their [solutions](exercises/cheat)


## Participant requirements

The course requires previous familiarity with python. All the concepts from the 
[python-intro course](https://gitlab.com/alba-synchrotron/controls-section/pythoncourse-intro) must be
clear to follow this one.

Familiarity with Object-Oriented programming in other languages will help you a 
lot, but it is *not* required.

The course requires **commitment**.  It consists of **4 sessions of ~5h each**. 
Some participants (depending on previous programming experience) may need to 
review / work on exercises before the next session.
**Attending to previous sessions is a strong requisite to be able to follow 
the next session**.

It also requires between 2.5h to 5h *before the course* to complete the 
pre-course check-list (see below).


## Pre-course checklist

**IMPORTANT**: Make sure to follow this checklist **before** starting the course:

- Follow [these instructions](https://gitlab.com/alba-synchrotron/controls-section/pythoncourse-intro/blob/master/pre-course.md)
  to install the needed software (python3, PyQt5, scipy, pyqtgraph and an IDE)
  **on your PC**. Note: For the intermediate course I strongly suggest to install [PyCharm Community Edition](https://www.jetbrains.com/pycharm)
  *(Expected required time: 20'-60'  , Priority: very high)*
  
- Dedicate *at least* 1 hour to reading **and understanding** [this article on
  Python OOP](https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/)
  (note: you can stop when you get to "Abstract Classes"). 
  *(Expected required time: 60'-180' Priority: very high)*
  
- Install git on your machine and clone this repo. If you still haven't heard 
  about git, please follow the Git beginners tutorial from: 
  http://backlogtool.com/git-guide/en/ (you will not regret learning git).
  **All course-related communication (passing exercises, etc will be done via 
  git)**
  *(Expected required time: 20'-60' Priority: high)*

- Have a quick look (for refreshing) at the materials and docs from the 
  [intro course](https://gitlab.com/alba-synchrotron/controls-section/pythoncourse-intro)
  *(Expected required time: 10'-30' , Priority: medium)*

